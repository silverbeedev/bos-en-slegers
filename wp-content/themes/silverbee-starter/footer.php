<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Silverbee_Starter
 */

?>

	</div><!-- #content -->
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="row justify-content-center">
            <div class="col-11 col-xl-10">
               <div class="row justify-content-between">
                   <div class="col-12 col-sm-6 align-self-center">
                       2015-2018 &copy; Copyright - Autogroep Bos & Slegers
                   </div>
                   <div class="col-12 col-sm-6 text-right">
                       <div class="sassy-social-wrapper">
                           <span>Deel deze pagina: </span>
                           <?php echo do_shortcode('[Sassy_Social_Share type="standard"]'); ?>
                       </div>
                       <a href="https://www.bosenslegers.nl/informatie/disclaimer/" target="_blank">Disclaimer</a>
                       <a href="https://www.bosenslegers.nl/informatie/privacy-policy/" target="_blank">Privacy Policy</a>
                       <a href="https://www.bosenslegers.nl" target="_blank">Website</a>
                   </div>
               </div>
            </div>
        </div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>
<script type="text/javascript">
    _linkedin_partner_id = "544148";
    window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];
    window._linkedin_data_partner_ids.push(_linkedin_partner_id);
</script><script type="text/javascript">
    (function(){var s = document.getElementsByTagName("script")[0];
        var b = document.createElement("script");
        b.type = "text/javascript";b.async = true;
        b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
        s.parentNode.insertBefore(b, s);})();
</script>
<noscript>
    <img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=544148&fmt=gif
https://dc.ads.linkedin.com/collect/?pid=544148&fmt=gif
" />
</noscript>
</body>
</html>
