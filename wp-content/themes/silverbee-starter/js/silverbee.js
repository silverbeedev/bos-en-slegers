(function ($) {
    $(document).ready(function(){

        $(".button, .header-cta").click(function() {
            $([document.documentElement, document.body]).animate({
                scrollTop: $("#contact-wrapper").offset().top
            }, 2000);
        });

        // Sets interval...what is transition slide speed?
        $('#bs-carousel').carousel({
            interval: 5000
        });

        $('.cases-slider').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,
            arrows: false,
            dots: false,
            pauseOnHover: false,
            responsive: [
                {
                    breakpoint: 960,
                    settings: {
                        slidesToShow: 4
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 3
                    }
                }, {
                    breakpoint: 520,
                    settings: {
                        slidesToShow: 1
                    }
                }]
        });

    });
})(jQuery);