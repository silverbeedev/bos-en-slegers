<?php
/**
 * The template for displaying the front-page
 *
 * @package Silverbee_Starter
 */

get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <section class="front-page">
                <section>
                    <div class="row justify-content-center">
                        <div class="col-11 col-xl-10">
                            <div class="bullet-wrapper">
                                <div class="row justify-content-center">
                                    <div class="col-8 col-sm-6 col-md-5 col-lg-5 col-xl-5 align-self-center">
                                        <div class="bullet-item-left">
                                            <img src="<?php echo get_template_directory_uri() ?>/dist/img/busje.png"
                                                 alt="">
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-7 col-lg-7 offset-0 col-xl-6 offset-xl-1 align-self-center">
                                        <div class="bullet-item-right">
                                            <h2>
                                                Our benefits
                                            </h2>
                                            <ul>
                                                <li>
                                                    18 tot 72 maanden zorgeloos leasen en
                                                    eenvoudig opzeggen!
                                                </li>
                                                <li>
                                                    Stel zelf het contract en de gewenste bedrijfswagen samen, wij
                                                    assiteren u graag bij de aanvraag van uw financiering of de
                                                    leaseconstructie!
                                                </li>
                                                <li>
                                                    Merkonafhankelijk, een ruim aanbod met bedrijfswagens tot 3500 kg,
                                                    inruilen van uw huidige bedrijfswagen of prive auto ook mogelijk!

                                                </li>
                                                <li>
                                                    Autogroep Bos en Slegers Bedrijfswagens Eindhoven beschikt over
                                                    een modern uitgeruste werkplaats met uitstluitend geceritficeerd
                                                    personeel!
                                                </li>
                                                <li>
                                                    Naast onderhoud bieden wij ook aanpassingsmogelijkheden van
                                                    bedrijfswagens zoals: betimmering, inrichting en reclame!
                                                </li>
                                            </ul>
                                            <div class="btn-cta button">
                                                Neem contact op
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section>
                    <div class="case-slider-wrapper">
                        <div class="row justify-content-center text-center">
                            <div class="col-12">
                                <h4>Ons aanbod:</h4>
                                <div class="cases-logos">
                                    <div class="cases-slider">
                                        <div class="case-logo">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/logos/citroen.png"/>
                                        </div>
                                        <div class="case-logo">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/logos/vw.png"/>
                                        </div>
                                        <div class="case-logo">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/logos/ford.png"/>
                                        </div>
                                        <div class="case-logo">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/logos/mercedes.png"/>
                                        </div>
                                        <div class="case-logo">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/logos/peugeot.png"/>
                                        </div>
                                        <div class="case-logo">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/logos/citroen.png"/>
                                        </div>
                                        <div class="case-logo">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/logos/vw.png"/>
                                        </div>
                                        <div class="case-logo">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/logos/ford.png"/>
                                        </div>
                                        <div class="case-logo">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/logos/mercedes.png"/>
                                        </div>
                                        <div class="case-logo">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/logos/peugeot.png"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section>
                    <div class="subinfo-wrapper">
                        <div class="row justify-content-center">
                            <div class="col-11 col-xl-10">
                                <div class="row">
                                    <div class="col-12 col-md-9 col-lg-7 col-xl-5">
                                        <div class="subinfo-item">
                                            <h2>
                                                Advies Leasing en Financiering  bedrijfswagens Eindhoven
                                            </h2>
                                                <p>
                                                    Bent u op zoek naar de beste deal om uw bedrijfswagen te financieren of leasen? Bij Autogroep Bos en Slegers kunt u kiezen uit financial & operational lease:
                                                </p>
                                                <p>
                                                    Financial lease: uw bedrijfswagen wordt gefinancierd, maar u wordt de officiële eigenaar.
                                                    Daardoor kunt u de investering afschrijven. U betaalt de kosten van de verzekering, het onderhoud en eventuele reparatie apart.
                                                </p>
                                                <p>
                                                    Bij Operational lease: u geeft alles uit handen – van verzekering tot belasting en onderhoud,
                                                    en als u wilt zelfs de brandstofkosten – en betaalt hiervoor een vast bedrag per maand. U wordt geen eigenaar van uw bedrijfswagen.
                                                </p>
                                            <div class="btn-cta invert button">
                                                Contact ons
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section>
                    <div class="row justify-content-center">
                        <div class="col-11 col-xl-10">
                            <div class="usp-wrapper">
                                <div class="row justify-content-center text-center">
                                    <div class="col-12 col-sm-10 col-md-4">
                                        <div class="usp-item">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/icons/usp-1.png"
                                                 alt="">
                                            <h3>
                                                Voor nieuw en tweedehands
                                            </h3>
                                            <p>
                                                Je kunt bedrijfswagens met een aanschafwaarde vanaf €5.000 leasen.
                                                Nieuw, maar ook tweedehands.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-10 col-md-4">
                                        <div class="usp-item">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/icons/usp-2.png"
                                                 alt="">
                                            <h3>
                                                Vast maandbedrag
                                            </h3>
                                            <p>
                                                Je betaalt maandelijks rente en aflossing. Je kunt eigen geld inbrengen
                                                als je
                                                het maandbedrag wilt verlagen, maar dat hoeft niet.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-10 col-md-4">
                                        <div class="usp-item">
                                            <img src="<?php echo get_template_directory_uri() ?>/img/icons/usp-3.png"
                                                 alt="">
                                            <h3>
                                                De bedrijfswagen is het onderpand
                                            </h3>
                                            <p>
                                                Je hoeft doorgaans geen eigen geld of zekerheden in te brengen.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section>
                    <div class="review-wrapper">
                        <div class="row justify-content-center">
                            <div class="col-11 col-xl-10">
                                <div id="review-carousel" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#review-carousel" data-slide-to="0" class="active"></li>
                                        <li data-target="#review-carousel" data-slide-to="1"></li>
                                        <li data-target="#review-carousel" data-slide-to="2"></li>
                                    </ol>
                                    <div class="carousel-inner">
                                        <div class="carousel-item active">
                                            <div class="item-content">
                                                <div class="row justify-content-center">
                                                    <div class="col-12 col-lg-10 col-xl-9">
                                                        <div class="score">
                                                            <span class="star filled"></span>
                                                            <span class="star filled"></span>
                                                            <span class="star filled"></span>
                                                            <span class="star filled"></span>
                                                            <span class="star filled"></span>
                                                        </div>
                                                        <h3>
                                                            Begeleid in het hele traject, mooie bedrijfswagen inclusief betimmering!
                                                        </h3>
                                                        <p class="reviewer">
                                                            Huub Stevens (2017)
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="carousel-item">
                                            <div class="item-content">
                                                <div class="row justify-content-center">
                                                    <div class="col-12 col-lg-10 col-xl-9">
                                                        <div class="score">
                                                            <span class="star filled"></span>
                                                            <span class="star filled"></span>
                                                            <span class="star filled"></span>
                                                            <span class="star filled"></span>
                                                            <span class="star empty"></span>
                                                        </div>
                                                        <h3>
                                                            Mijn oude bedrijfswagen ingeruild en een voordelige nieuwe
                                                            mee naar huis mogen nemen!
                                                        </h3>
                                                        <p class="reviewer">
                                                            Ronald Dircx (2018)
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="carousel-item">
                                            <div class="item-content">
                                                <div class="row justify-content-center">
                                                    <div class="col-12 col-lg-10 col-xl-9">
                                                        <div class="score">
                                                            <span class="star filled"></span>
                                                            <span class="star filled"></span>
                                                            <span class="star filled"></span>
                                                            <span class="star filled"></span>
                                                            <span class="star filled"></span>
                                                        </div>
                                                        <h3>
                                                            De bedrijfsauto eerst gezien via Facetime en vervolgens naar showroom gekomen, erg efficient!
                                                        </h3>
                                                        <p class="reviewer">
                                                            Yolanda van Beek (2018)
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section>
                    <div class="contact-wrapper" id="contact-wrapper">
                        <div class="row justify-content-center">
                            <div class="col-11 col-xl-10">
                                <div class="row">
                                    <div class="col-12 text-center">
                                        <h2>
                                            Wij helpen u graag vrijblijvend bij het vinden van de beste oplossing!
                                        </h2>
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <div class="contact-item left">
											<?php echo do_shortcode( '[contact-form-7 id="5" title="Contactformulier 1"]' ); ?>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <div class="contact-item right">
                                            <div class="contact-info-overlay">
                                                <p>
                                                    <strong>Contactinformatie:</strong>
                                                </p>
                                                <span>
                                                    <a href="tel:+31(0)40 252 24 25">
                                                        T: +31 (0)40 252 24 25
                                                    </a>
                                                </span>
                                                <span>
                                                    <a href="tel:+31(0)40 251 19 52">
                                                        Fax: +31 (0)40 251 19 52
                                                    </a>
                                                </span>
                                                <span>
                                                    <a href="mailto:info@bosenslegers.nl">E: info@bosenslegers.nl</a>
                                                </span>
                                            </div>
                                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d9938.430737690733!2d5.4259417!3d51.4837151!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3e671a33215e9624!2sAutobedrijf+Bos+en+Slegers+Auto+Eindhoven!5e0!3m2!1snl!2snl!4v1539610633575"
                                                    width="100%" height="450" frameborder="0" style="border:0"
                                                    allowfullscreen></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section>
                    <div class="facetime-wrapper">
                        <div class="row">
                            <div class="col-12 text-center">
                                <a href="tel:+31620490119">
                                    <img src="<?php echo get_template_directory_uri()?>/img/logos/facetime.png" alt="facetime">
                                </a>
                                <p>
                                    <a href="tel:+31620490119">
                                        Geen tijd om langs te komen? Start een Facetime-gesprek met ons!
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </section>

            </section><!-- .front-page -->
        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_footer();
