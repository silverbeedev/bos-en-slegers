<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Silverbee_Starter
 */

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <!--    get analytics script-->
	<?php get_template_part( 'template-parts/analytics/content', 'head' ); ?>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<!--    get analytics script-->
<?php get_template_part( 'template-parts/analytics/content', 'body' ); ?>
<div id="page" class="site">
    <a class="skip-link screen-reader-text"
       href="#content"><?php esc_html_e( 'Skip to content', 'silverbee-starter' ); ?></a>

    <header id="masthead" class="site-header" role="banner">
        <section>
            <div class="row justify-content-center">
                <div class="col-xl-4 col-lg-5 col-md-4 col-sm-4 col-5">
                    <div class="site-branding">
                        <img src="<?php echo get_template_directory_uri() ?>/dist/img/brand.png" alt="">
                    </div><!-- .site-branding -->
                </div>
            </div>
        </section>

		<?php if ( ! is_page( 'bedankt' ) ) : ?>
            <section>
                <div class="row">
                    <div class="col-12">
                        <div id="bs-carousel" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#bs-carousel" data-slide-to="0" class="active"></li>
                                <li data-target="#bs-carousel" data-slide-to="1"></li>
                                <li data-target="#bs-carousel" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="carousel-item active"
                                     style="background: linear-gradient(to right, rgba(0,0,0,0.50) 0%, rgba(0,0,0,0.50) 100%), url('wp-content/themes/silverbee-starter/dist/img/vw.jpg')">
                                    <div class="item-content">
                                        <div class="row">
                                            <div class="col-sm-12 offset-sm-0 col-md-8 offset-md-2 col-lg-7 col-xl-4">
                                                <h1>Wij garanderen uw mobiliteit</h1>
                                                <p>
                                                    Korte lijnen, Betrouwbaar, Betrokken en Oplossingsgericht daar staan
                                                    we
                                                    voor!
                                                </p>
                                                <div class="slider-cta button">
                                                    Contact ons
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="carousel-item"
                                     style="background: linear-gradient(to right, rgba(0,0,0,0.50) 0%, rgba(0,0,0,0.50) 100%), url('wp-content/themes/silverbee-starter/dist/img/slide02.jpg')">
                                    <div class="item-content">
                                        <div class="row">
                                            <div class="col-sm-12 offset-sm-0 col-md-8 offset-md-2 col-lg-7 col-xl-4">
                                                <h1>Je hebt investeren. En je hebt slim investeren. Verschil moet er
                                                    zijn.</h1>
                                                <p>
                                                    Wie elke dag hard werkt, verdient best wat extra luxe.
                                                </p>
                                                <div class="slider-cta button">
                                                    Contact ons
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="carousel-item"
                                     style="background: linear-gradient(to right, rgba(0,0,0,0.25) 0%, rgba(0,0,0,0.25) 100%), url('wp-content/themes/silverbee-starter/dist/img/peugeot.jpg')">
                                    <div class="item-content">
                                        <div class="row">
                                            <div class="col-sm-12 offset-sm-0 col-md-8 offset-md-2 col-lg-7 col-xl-4">
                                                <p>
                                                    Bent u opzoek naar een nieuwe bedrijfswagen, maar wilt u uw oude
                                                    verkopen?
                                                    Inruilen van uw bedrijfswagen of privé mogelijk!
                                                </p>
                                                <div class="slider-cta button">
                                                    Contact ons
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#bs-carousel" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#bs-carousel" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                        <div class="header-cta">
                                <span>
                                    Leasen vanaf &euro;95.-
                                </span>
                        </div>
                    </div>
                </div>
            </section>

		<?php endif; ?>

    </header><!-- #masthead -->

    <div id="content" class="site-content">
